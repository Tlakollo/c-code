/*
 * C program to implement stack. Stack is a LIFO data structure.
 * Stack operations: PUSH(insert operation), POP(Delete operation),
 * TOP (check top of stack value) and ISEMPTY functions of a stack.
 */
#include <stdio.h>
#define MAXSIZE 5
 
struct stack
{
    int stk[MAXSIZE];
    int top;
};
typedef struct stack STACK;
STACK s;
 
void push(void);
int  pop(void);
void display(void);
 

/*  Function to add an element to the stack */
void push (int num)
{
    if (s.top == (MAXSIZE - 1))
    {
        printf ("Stack is Full\n");
        return;
    }
    else
    {
        s.stk[++s.top] = num;
    }
    return;
}
/*  Function to delete an element from the stack */
int pop ()
{
    int num;
    if (s.top == - 1)
    {
        printf ("Stack is Empty\n");
        return (s.top);
    }
    else
    {
        num = s.stk[s.top];
        printf ("poped element is = %dn", s.stk[s.top]);
        s.top = s.top - 1;
    }
    return(num);
}
/*  Function to display the top value of the stack */
int top ()
{
    int i;
    if (s.top == -1)
    {
        printf ("The status of the stack is empty")
        return -1;
    }
    else
    {
        return s.stk[s.top];
    }
}
/*  Function to display if the stack is empty */
int isEmpty()
{
    if (s.top == -1)
    {
        printf ("The status of the stack is empty")
        return 0;
    }
    else
    {
        printf ("The status of the stack is not empty")
        return 1;
    }
}