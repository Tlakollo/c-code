#include <stdio.h>

void swap (int array[], int a, int b){
    int temp;
    temp = array[a];
    array[a]=array[b];
    array[b]=temp;
}
void DivideArray(int array[],int left, int right, int pivot){
    int j = right;
    int i = left;
    while (i<j){
        while(array[i]<=array[pivot]&&(i!=pivot)){
            i++;
        }
        while(array[j]>=array[pivot]&&(j!=pivot)){
            j--;
        }
        swap(array,i,j);
        if(i==pivot){
            pivot=j;
        }
        else if(j==pivot){
            pivot=i;
        }
    }
    DivideArray(array,left,pivot,(pivot-left)/2);
    DivideArray(array,pivot,right,(right-pivot)/2);
}
void QuickSort(int array[], int size){
    //declaring variables
    int pivot = (size-1)/2;
    int left = 0;
    int right = size-1;
    DivideArray(array, left, right, pivot);
}
int main() {
	int myarray [8]={0,5,9,1,3,4,10,2};
	QuickSort(myarray,(sizeof(myarray)/sizeof(int)));
	for(int i=0;i<(sizeof(myarray)/sizeof(int));i++){
	    printf("%d, ",myarray[i]);
	}
	//should print 0, 1, 2, 3, 4, 5, 9, 10, 
}